import os
import requests

from django.shortcuts import render, redirect, HttpResponse
from requests.exceptions import ConnectionError


API_URL = os.getenv('API_URL')
CLIENT_ID = os.getenv('CLIENT_ID')
CLIENT_SECRET = os.getenv('CLIENT_SECRET')
CLIENT_USER = os.getenv('CLIENT_USER')
CLIENT_PW = os.getenv('CLIENT_PW')
GRANT_TYPE = os.getenv('GRANT_TYPE')


def home(request):
    template = 'mysite/home.html'
    context = {}

    user = request.session.get("USER")
    session = request.session.get("API_SESSION")

    if not user and not session:
        return redirect("login")

    context['host'] = API_URL
    context['user'] = user
    context['user_id'] = user.get('id')
    context['user_fname'] = user.get('first_name')
    context['user_lname'] = user.get('last_name')
    context['token'] = request.session["API_SESSION"]["access_token"]

    if request.method == "GET":
        url = "{url}/api/member/{id}/members/".format(
            url=API_URL, id=user.get('id'))

        headers = {'Authorization': 'Bearer ' +
                   request.session["API_SESSION"]["access_token"]}
        data = {
            "username": request.POST.get('username'),
            "password": request.POST.get('password')
        }
        r = requests.get(url, headers=headers)

        if r.status_code == 200:
            response = r.json()
            context['members'] = response

    return render(request, template, context)


def login_view(request):
    template = 'mysite/login.html'
    context = {}

    user = request.session.get("USER")
    if user:
        return redirect("home")

    try:
        if request.method == "POST":
            request_body = request.POST
            username = request_body.get('username')
            password = request_body.get('password')
            url = "{}/o/token/".format(API_URL)
            data = {
                "username": username,
                "password": password,
                "grant_type": GRANT_TYPE,
                "client_id": CLIENT_ID,
                "client_secret": CLIENT_SECRET
            }

            r = requests.post(url, data=data)
            if r.status_code == 200:
                session = request.session["API_SESSION"] = r.json()
                access_token = session['access_token']
                headers = {'Authorization': 'Bearer ' + access_token}
                user = get_user(request_body, headers)
                request.session["USER"] = user
                return redirect("home")
            else:
                context['error_msg'] = "Incorrect Username or Password."
    except ConnectionError as e:
        context['error_msg'] = "Cannot connect to server. Try again in a few minutes..."

    return render(request, template, context)


def logout_view(request):

    url = '{}/o/revoke_token/'.format(API_URL)
    token = request.session["API_SESSION"]["access_token"]
    data = {
        "token": token,
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET
    }

    r = requests.post(url, data=data)

    if r.status_code == 200:
        request.session.flush()
        return redirect("login")


def signup_view(request):
    template = 'mysite/register.html'
    context = {}

    user = request.session.get("USER")
    if user:
        return redirect("home")

    if request.method == "POST":
        request_body = request.POST
        username = request_body.get('username')
        email = request_body.get('email')
        password1 = request_body.get('password1')
        password2 = request_body.get('password2')
        first_name = request_body.get('first_name')
        last_name = request_body.get('last_name')
        dob = request_body.get('dob')

        cached_form = request.session["SIGNUP_FORM"] = {
            "username": username,
            "email": email,
            "first_name": first_name,
            "last_name": last_name,
            "dob": dob
        }

        if password1 != password2:
            context['cached_form'] = cached_form
            context['error_msg'] = "Passwords do not match."
            return render(request, template, context)

        url = '{}/o/token/'.format(API_URL)
        data = {
            "grant_type": GRANT_TYPE,
            "username": CLIENT_USER,
            "password": CLIENT_PW,
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET
        }

        token_r = requests.post(url, data=data)

        if token_r.status_code == 200:
            session = request.session["API_SESSION"] = token_r.json()
            access_token = session['access_token']
            headers = {'Authorization': 'Bearer ' + access_token}

            register_url = '{}/api/account/register/'.format(API_URL)
            data = {
                "username": username,
                "email": email,
                "password": password1,
                "password2": password2,
                "first_name": first_name,
                "last_name": last_name,
                "dob": dob
            }

            r = requests.post(register_url, data=data, headers=headers)
            response = r.json()

            if r.status_code == 200:
                user = get_user(request_body, headers)
                member = add_member(request, user, headers)
                return redirect('login')
            else:
                context['error_msg'] = "Unable to register user. Please try again."
        else:
            context['error_msg'] = "Please try again."

    return render(request, template, context)


def account_view(request):
    template = 'mysite/account.html'
    context = {}

    user = request.session.get("USER")
    if not user:
        return redirect("login")

    if request.method == "POST":
        request_body = request.POST
        username = user.get('username')
        email = request_body.get('email')
        first_name = request_body.get('first_name')
        last_name = request_body.get('last_name')
        dob = request_body.get('dob')

        url = '{url}/api/account/{id}/update/'.format(
            url=API_URL, id=user.get('id'))
        headers = {'Authorization': 'Bearer ' +
                   request.session["API_SESSION"]["access_token"]}
        data = {
            "username": username,
            "email": email,
            "first_name": first_name,
            "last_name": last_name,
            "dob": dob
        }

        r = requests.put(url, data=data, headers=headers)
        response = r.json()

        if r.status_code == 200:
            request.session["USER"]["email"] = email
            request.session["USER"]["first_name"] = first_name
            request.session["USER"]["last_name"] = last_name
            request.session["USER"]["dob"] = dob
            context['success'] = "User has been updated."
        else:
            context['error_msg'] = "Failed to update user."

    context['user'] = user

    return render(request, template, context)


def change_password_view(request):

    template = 'mysite/changepass.html'
    context = {}

    request_body = request.POST
    old_password = request_body.get("old_password")
    new_password = request_body.get("new_password")
    confirm_new_password = request_body.get("confirm_new_password")

    if request.method == "POST":
        url = '{}/api/account/changepassword/'.format(API_URL)
        data = {
            "old_password": old_password,
            "new_password": new_password,
            "confirm_new_password": confirm_new_password
        }
        headers = {'Authorization': 'Bearer ' +
                   request.session["API_SESSION"]["access_token"]}

        r = requests.put(url, data=data, headers=headers)
        response = r.json()
        message = response['response']
        if r.status_code == 200:
            print(response)
            context['success'] = message
        else:
            context['error_msg'] = message

    return render(request, template, context)

# Family Members


def add_family_member(request):

    template = 'mysite/addmember.html'
    context = {}

    if request.method == "POST":
        request_body = request.POST
        user_id = request.session["USER"]["id"]
        first_name = request_body.get('first_name')
        last_name = request_body.get('last_name')
        dob = request_body.get('dob')
        relation = request_body.get('relation')
        vital_status = request_body.get('vital_status')

        url = '{url}/api/member/{id}/add_new/'.format(url=API_URL, id=user_id)
        data = {
            "user_id": user_id,
            "first_name": first_name,
            "last_name": last_name,
            "dob": dob,
            "relation": relation,
            "vital_status": vital_status
        }
        headers = {'Authorization': 'Bearer ' +
                   request.session["API_SESSION"]["access_token"]}

        r = requests.post(url, data=data, headers=headers)
        response = r.json()
        if r.status_code == 200:
            return redirect('home')

    return render(request, template, context)


def member_view(request, pk):
    template = 'mysite/editmember.html'
    context = {}

    url = '{url}/api/member/{id}/'.format(url=API_URL, id=pk)
    headers = {'Authorization': 'Bearer ' +
               request.session["API_SESSION"]["access_token"]}

    r = requests.get(url, headers=headers)
    response = r.json()

    if r.status_code == 200:
        context['member'] = response
    else:
        return redirect('home')

    if request.method == "POST":
        request_body = request.POST
        first_name = request_body.get("first_name")
        last_name = request_body.get("last_name")
        dob = request_body.get("dob")
        relation = request_body.get("relation")
        vital_status = request_body.get("vital_status")

        update_url = '{url}/api/member/update/{id}/'.format(url=API_URL, id=pk)
        headers = {'Authorization': 'Bearer ' +
                   request.session["API_SESSION"]["access_token"]}
        data = {
            "first_name": first_name,
            "last_name": last_name,
            "dob": dob,
            "relation": relation,
            "vital_status": vital_status
        }
        update_r = requests.put(update_url, data=data, headers=headers)
        update_response = update_r.json()

        if update_r.status_code == 200:
            context['success'] = "Family Member detail has been successfully updated."
            context['member'] = update_response
        else:
            context['error_msg'] = update_response

    return render(request, template, context)


def get_user(request, headers):

    username = request.get('username')
    url = '{}/api/account/get_user/'.format(API_URL)
    data = {
        "username": username
    }

    r = requests.get(url, data=data, headers=headers)

    if r.status_code == 200:
        response = r.json()
        return response


def add_member(request, user, headers):
    url = '{url}/api/member/{id}/add_new/'.format(
        url=API_URL, id=user.get('id'))
    response = None
    data = {}

    if user:
        data['user_id'] = user.get('id')
        data['first_name'] = user.get('first_name')
        data['last_name'] = user.get('last_name')
        data['dob'] = user.get('dob')
        data['relation'] = "me"
        data['vital_status'] = "alive"

        r = requests.post(url, data=data, headers=headers)
        response = r.json()

    return response
